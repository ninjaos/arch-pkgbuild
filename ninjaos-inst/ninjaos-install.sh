#!/usr/bin/env bash
# Post install configuration script that runs inside th

# Set this to whatever local you want.
locale=en_US.UTF-8
post_install() {
  main
}

message() {
  echo "Ninja OS: ${@}"
}

warn(){
  echo "Ninja OS: Warn: ${@}"
}
_generate_locale() {
  local -i local_errors=0
  cp /etc/locale.gen /etc/locale.gen.bak || local_errors+=1
  echo "${locale}.UTF-8 UTF-8" > /etc/locale.gen || local_errors+=1
  locale-gen || local_errors+=1
  mv /etc/locale.gen.bak /etc/locale.gen || local_errors+=1
  return $local_errors
}

_install_metasploit() {
  # initialize the postgresql database, make a user for metasploit, and then
  # cache the database
  local -i local_errors=0
  local msf_password="n1nj40s"

  # initialize the postgresql database and start it
  sudo -u postgres initdb --locale $LANG -E UTF8 -D '/var/lib/postgres/data' || local_errors+=1
  cd /var/lib/postgres
  mkdir -p /run/postgresql
  chown postgres /run/postgresql
  sudo -u postgres pg_ctl -D /var/lib/postgres/data start || local_errors+=1
  sleep 5 # wait 5 seconds for the database to load

  #create a database for msf, and a user called metasploit
  sudo -u postgres psql << EOF
  create database msf;
  create user metasploit with encrypted password \'${msf_password}\';
  grant all privileges on database msf to metasploit;
EOF
  local_errors+=$?

  #make temp database confile file and then cache the database
  mkdir -p /root/.msf4
  cat > /root/.msf4/database.yml << EOF
production:
 adapter: postgresql
 database: msf
 username: metasploit
 password: ${msf_password}
 host: localhost
 port: 5432
 pool: 5
 timeout: 5
EOF
  local_errors+=$?
  #now, start metasploit and rebuild the cache
  MSF_DATABASE_CONFIG=/root/.msf4/database.yml
  msfconsole --quiet -x db_rebuild_cache << EOF
EOF
  #local_errors+=$?

  #clean up
  rm -rf /root/.msf4/
  pg_ctl -D /var/lib/postgres/data stop
  pkill msfrpcd
  return $local_errors
}

_net_cap_permissions() {
  message "Setting libcap capabilities for non-root network apps."
  local net_apps="dumpcap arp-scan traceroute"
  local -i local_errors=0
  for app in ${net_apps};do
    setcap 'CAP_NET_RAW+eip CAP_NET_ADMIN+eip' /usr/bin/${app} || local_errors+=1
  done
  ## New advances in aufs allow the use of XATTR and with it, libcap, this
  # should be obsolete
  # Set SUID so ping actually works 
  #message "setting SUID bit for ping,arp-scan, and dumpcap"
  #chmod u+s /usr/bin/ping
  #errors+=$?
  #chmod u+s /usr/bin/dumpcap
  #errors+=$?
  #chmod u+s /usr/bin/arp-scan
  #errors+=$?
  
  return ${local_errors}
}

main() {
  message "Starting Post-Install Script..."

  #keep a count of errors
  local -i errors=0 
  #initialize pacman's gpg keys

  message "Generating Package GPG-Keyring"
  pacman-key --init || errors+=1
  pacman-key --populate archlinux || errors+=1
  
  message "Setting libcap capabilities for non-root network apps."
  _net_cap_permissions || errors+=1

  message "Configuring Network and Interfaces"
  # Use old style network interface naming conventions(i.e. eth0/wlan0)
  ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules || errors+=1
  ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime || errors+=1
  # Add a system group for NetworkManager
  groupadd -r networkmanager || errors+=1

  #Disable Root Login. User must use sudo
  message "Locking root account"
  usermod -L root || errors+=1

  message "Disabling Uneeded Services"
  # Generate Dynamic Link Cache, and prevent it from running at boot
  ldconfig -X || errors+=1
  systemctl mask mkinitcpio-generate-shutdown-ramfs.service || errors+=1
  systemctl mask ldconfig.service || errors+=1
  systemctl mask man-db.service || errors+=1

  message "Generating Locale"
  _generate_locale || errors+=1

  message "Configuring Metasploit Postresql Database"
  _install_metasploit
  if [ $? -ne 0 ];then
    message "Metasploit Database Configuration Failed!"
    errors+=1
  fi

  # Now check if there are any errors to determine the type of exit message
  if [ ${errors} -eq 0 ];then
    message "Install Script: Done, No Errors"
      else
    message "Install Script: Something Went Wrong with the Post Install: ${errors} error(s)"
  fi
}

