Ninja OS Arch Packages
======================

This repo is for Arch Linux packages developed for and by Ninja OS

CORE
----
ninjaos-init	- Dummy package that runs a script inside a fresh chroot of Ninja OS

ninjaos-keyring	- pacman keyring package for the \[ninjaos\] repo


SUB-Projects
------------
ninja-bootandnuke - Boot and Nuke script and profile for mkinitcpio. Securely erases all data of all storage media. Also includes a tool for making a "shuriken" or a boot and nuke image on a USB stick with a minimal syslinux bootloader

All packages are licensced under the GNU Public License(GPL) Version 3
https://www.gnu.org/licenses/gpl-3.0.txt


